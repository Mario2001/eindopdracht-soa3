package domain.devops;

import adapter.sender.MessageSender;

public class LeadDeveloper extends Developer {
    public LeadDeveloper(String name, String email, String phoneNumber, MessageSender messageSender) {
        super(name, email, phoneNumber, messageSender);
    }
}
