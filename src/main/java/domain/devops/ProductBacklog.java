package domain.devops;

import java.util.ArrayList;
import java.util.List;

public class ProductBacklog {
    private List<ProductBacklogItem> productBacklogItems;

    public ProductBacklog() {
        this.productBacklogItems = new ArrayList<>();
    }

    public void moveProductBacklogItemToSprintBacklog(ProductBacklogItem productBacklogItem, SprintBacklog sprintBacklog) {
        // Remove the product backlog item from the product backlog.
        productBacklogItems.remove(productBacklogItem);

        // Add the product backlog item to the sprint backlog.
        sprintBacklog.add(productBacklogItem);
    }

    public List<ProductBacklogItem> getProductBacklogItems() {
        return productBacklogItems;
    }

    public ProductBacklogItem getProductBacklogItem(int i) {
        return productBacklogItems.get(i);
    }

    public void add(ProductBacklogItem productBacklogItem) {
        productBacklogItems.add(productBacklogItem);
    }

    public void removeProductBacklogItem(int i) {
        productBacklogItems.remove(i);
    }
}
