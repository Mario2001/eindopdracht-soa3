package domain.devops;

import observer.Observer;
import observer.Subject;
import state.product_backlog_item.*;

import java.util.ArrayList;
import java.util.List;

public class ProductBacklogItem extends Subject {
    private String name;
    private String description;
    private int storyPoints;
    private SprintBacklog sprintBacklog;
    private ScrumTeamMember scrumTeamMember;
    private List<ProductBacklogItemActivity> productBacklogItemActivities;
    private List<DiscussionThread> discussionThreads;

    // Product Backlog Item states
    private ProductBacklogItemState toDoState;
    private ProductBacklogItemState doingState;
    private ProductBacklogItemState readyForTestingState;
    private ProductBacklogItemState testingState;
    private ProductBacklogItemState testedState;
    private ProductBacklogItemState doneState;

    private ProductBacklogItemState currentState;

    public ProductBacklogItem(String name, String description, int storyPoints) {
        this.name = name;
        this.description = description;
        this.storyPoints = storyPoints;
        this.productBacklogItemActivities = new ArrayList<>();
        this.discussionThreads = new ArrayList<>();

        // Product Backlog Item states
        toDoState = new ToDoState(this);
        doingState = new DoingState(this);
        readyForTestingState = new ReadyForTestingState(this);
        testingState = new TestingState(this);
        testedState = new TestedState(this);
        doneState = new DoneState(this);

        currentState = toDoState;
    }

    public void setToToDo() {
        currentState.setToTodo();
    }

    public void setToDoing(ScrumTeamMember scrumTeamMember) {
        currentState.setToDoing(scrumTeamMember);
    }

    public void setToReadyForTesting() {
        currentState.setToReadyForTesting();
    }

    public void setToTesting() {
        currentState.setToTesting();
    }

    public void setToTested() {
        currentState.setToTested();
    }

    public void setToDone() {
        currentState.setToDone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStoryPoints() {
        return storyPoints;
    }

    public void setStoryPoints(int storyPoints) {
        this.storyPoints = storyPoints;
    }

    public SprintBacklog getSprintBacklog() {
        return sprintBacklog;
    }

    public void setSprintBacklog(SprintBacklog sprintBacklog) {
        // A new product backlog item may not have a sprint backlog yet.
        // Therefore the registration of the scrum master and each tester is done in setSprintBacklog().

        // Register the scrum master as an observer.
        registerObserver(sprintBacklog.getSprint().getScrumMaster());

        // Register each tester as an observer.
        for (ScrumTeamMember scrumTeamMember : sprintBacklog.getSprint().getScrumTeamMembers()) {
            if (scrumTeamMember instanceof Tester) {
                registerObserver(scrumTeamMember);
            }
        }

        this.sprintBacklog = sprintBacklog;
    }

    public ScrumTeamMember getScrumTeamMember() {
        return scrumTeamMember;
    }

    public void setScrumTeamMember(ScrumTeamMember scrumTeamMember) {
        this.scrumTeamMember = scrumTeamMember;
    }

    public List<ProductBacklogItemActivity> getProductBacklogItemActivities() {
        return productBacklogItemActivities;
    }

    public ProductBacklogItemActivity getProductBacklogItemActivity(int i) {
        return productBacklogItemActivities.get(i);
    }

    public void add(ProductBacklogItemActivity productBacklogItemActivity) {
        productBacklogItemActivities.add(productBacklogItemActivity);
    }

    public void removeProductBacklogItemActivity(int i) {
        productBacklogItemActivities.remove(i);
    }

    public List<DiscussionThread> getDiscussionThreads() {
        return discussionThreads;
    }

    public DiscussionThread getDiscussionThread(int i) {
        return discussionThreads.get(i);
    }

    public void add(DiscussionThread discussionThread) {
        if (currentState.getClass() != DoneState.class) {
            discussionThreads.add(discussionThread);
        }
    }

    public void removeDiscussionThread(int i) {
        if (currentState.getClass() != DoneState.class) {
            discussionThreads.remove(i);
        }
    }

    public ProductBacklogItemState getToDoState() {
        return toDoState;
    }

    public ProductBacklogItemState getDoingState() {
        return doingState;
    }

    public ProductBacklogItemState getReadyForTestingState() {
        return readyForTestingState;
    }

    public ProductBacklogItemState getTestingState() {
        return testingState;
    }

    public ProductBacklogItemState getTestedState() {
        return testedState;
    }

    public ProductBacklogItemState getDoneState() {
        return doneState;
    }

    public ProductBacklogItemState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(ProductBacklogItemState state) {
        currentState = state;
    }

    @Override
    public void notifyObservers(String message) {
        for (Observer observer : observers) {
            observer.update(this, message);
        }
    }
}
