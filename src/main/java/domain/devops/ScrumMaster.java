package domain.devops;

import adapter.sender.MessageSender;

public class ScrumMaster extends ScrumTeamMember {

    public ScrumMaster(String name, String email, String phoneNumber, MessageSender messageSender) {
        super(name, email, phoneNumber, messageSender);
    }
}
