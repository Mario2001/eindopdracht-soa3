package domain.devops;

import observer.Observer;
import observer.Subject;
import state.product_backlog_item.DoneState;

import java.util.ArrayList;
import java.util.List;

public class DiscussionThread extends Subject {
    private String subject;
    private String description;
    private ProductBacklogItem productBacklogItem;
    private List<Comment> comments;

    public DiscussionThread(String subject, String description, ProductBacklogItem productBacklogItem) {
        this.subject = subject;
        this.description = description;
        this.productBacklogItem = productBacklogItem;
        this.comments = new ArrayList<>();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductBacklogItem getProductBacklogItem() {
        return productBacklogItem;
    }

    public void setProductBacklogItem(ProductBacklogItem productBacklogItem) {
        this.productBacklogItem = productBacklogItem;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public Comment getComment(int i) {
        return comments.get(i);
    }

    public void add(Comment comment) {
        if (productBacklogItem.getCurrentState().getClass() != DoneState.class) {
            comments.add(comment);

            // Register the scrum team member as an observer if not already.
            if(!observers.contains(comment.getScrumTeamMember())) {
                registerObserver(comment.getScrumTeamMember());
            }

            notifyObservers("New message in " + getSubject() + ": " +  comment.getMessage());
        }
    }

    public void removeComment(int i) {
        if (productBacklogItem.getCurrentState().getClass() != DoneState.class) {
            comments.remove(i);
        }
    }

    @Override
    public void notifyObservers(String message) {
        for (Observer observer : observers) {
            observer.update(this, message);
        }
    }
}
