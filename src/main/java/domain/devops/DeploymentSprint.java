package domain.devops;

import strategy.export.SprintExportType;

import java.time.LocalDateTime;
import java.util.List;

public class DeploymentSprint extends Sprint {
    public DeploymentSprint(Project project, String sprintName, String sprintGoal, LocalDateTime startDate, LocalDateTime endDate, SprintBacklog sprintBacklog, ScrumMaster scrumMaster, List<ScrumTeamMember> scrumTeamMembers, SprintExportType sprintExportType) {
        super(project, sprintName, sprintGoal, startDate, endDate, sprintBacklog, scrumMaster, scrumTeamMembers, sprintExportType);
    }
}
