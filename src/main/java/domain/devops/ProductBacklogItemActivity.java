package domain.devops;

public class ProductBacklogItemActivity {
    private String name;
    private String description;
    private ScrumTeamMember scrumTeamMember;

    public ProductBacklogItemActivity(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ScrumTeamMember getScrumTeamMember() {
        return scrumTeamMember;
    }

    public void setScrumTeamMember(ScrumTeamMember scrumTeamMember) {
        this.scrumTeamMember = scrumTeamMember;
    }
}
