package domain.devops;

import adapter.sender.MessageSender;

public class Developer extends ScrumTeamMember {
    public Developer(String name, String email, String phoneNumber, MessageSender messageSender) {
        super(name, email, phoneNumber, messageSender);
    }
}
