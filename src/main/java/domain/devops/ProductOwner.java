package domain.devops;

import adapter.sender.MessageSender;

public class ProductOwner extends ScrumTeamMember {

    public ProductOwner(String name, String email, String phoneNumber, MessageSender messageSender) {
        super(name, email, phoneNumber, messageSender);
    }
}
