package domain.devops;

import java.util.ArrayList;
import java.util.List;

public class SprintBacklog {
    private Sprint sprint;
    private List<ProductBacklogItem> productBacklogItems;

    public SprintBacklog(Sprint sprint) {
        this.sprint = sprint;
        this.productBacklogItems = new ArrayList<>();
    }

    public void moveProductBacklogItemToProductBacklog(ProductBacklogItem productBacklogItem) {
        // Remove the product backlog item from the sprint backlog.
        productBacklogItems.remove(productBacklogItem);

        // Add the product backlog item to the product backlog.
        sprint.getProject().getProductBacklog().add(productBacklogItem);
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public List<ProductBacklogItem> getProductBacklogItems() {
        return productBacklogItems;
    }

    public ProductBacklogItem getProductBacklogItem(int i) {
        return productBacklogItems.get(i);
    }

    public void add(ProductBacklogItem productBacklogItem) {
        productBacklogItems.add(productBacklogItem);
        productBacklogItem.getScrumTeamMember().add(productBacklogItem);
    }

    public void removeProductBacklogItem(int i) {
        productBacklogItems.remove(i);
    }
}
