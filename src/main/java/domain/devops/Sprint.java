package domain.devops;

import observer.Observer;
import observer.Subject;
import state.sprint.*;
import strategy.export.SprintExportStrategy;
import strategy.export.SprintExportType;
import strategy.export.SprintPDFExportStrategy;
import strategy.export.SprintPNGExportStrategy;
import template.pipeline.DevelopmentPipeline;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public abstract class Sprint extends Subject {
    private Project project;
    private String sprintName;
    private String sprintGoal;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private SprintBacklog sprintBacklog;
    private ScrumMaster scrumMaster;
    private List<ScrumTeamMember> scrumTeamMembers;

    private DevelopmentPipeline developmentPipeline;

    private SprintExportStrategy sprintExportStrategy;

    // Sprint states
    private SprintState createdState;
    private SprintState inProgressState;
    private SprintState finishedState;
    private SprintState reviewState;
    private SprintState failedReleaseState;
    private SprintState cancelledReleaseState;
    private SprintState closedState;

    private SprintState currentState;

    private Timer timer;

    public Sprint(Project project, String sprintName, String sprintGoal, LocalDateTime startDate, LocalDateTime endDate, SprintBacklog sprintBacklog, ScrumMaster scrumMaster, List<ScrumTeamMember> scrumTeamMembers, SprintExportType sprintExportType) {
        this.project = project;
        this.sprintName = sprintName;
        this.sprintGoal = sprintGoal;
        this.startDate = startDate;
        this.endDate = endDate;
        this.sprintBacklog = sprintBacklog;
        this.scrumMaster = scrumMaster;
        this.scrumTeamMembers = scrumTeamMembers;

        // Register the product owner as an observer.
        registerObserver(project.getProductOwner());

        // Register the scrum master as an observer.
        registerObserver(scrumMaster);

        // Register each scrum team member as an observer.
        for (ScrumTeamMember scrumTeamMember : scrumTeamMembers) {
            registerObserver(scrumTeamMember);
        }

        if (sprintExportType == SprintExportType.PDF) {
            this.sprintExportStrategy = new SprintPDFExportStrategy();
        } else if (sprintExportType == SprintExportType.PNG) {
            this.sprintExportStrategy = new SprintPNGExportStrategy();
        }

        // Sprint states
        createdState = new CreatedState(this);
        inProgressState = new InProgressState(this);
        finishedState = new FinishedState(this);
        reviewState = new ReviewState(this);
        failedReleaseState = new FailedReleaseState(this);
        cancelledReleaseState = new CancelledReleaseState(this);
        closedState = new ClosedState(this);

        currentState = createdState;

        timer = new Timer(true);
    }

    public void startSprint() {
        currentState.startSprint();

        Date date = Date.from(endDate.atZone(ZoneId.systemDefault()).toInstant());
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                currentState.finishSprint();
            }
        }, date);
    }

    public void finishSprint() {
        timer.cancel();
        currentState.finishSprint();
    }

    public void reviewSprint() {
        currentState.reviewSprint();
    }

    public void releaseSprint() {
        currentState.releaseSprint();
    }

    public void cancelSprintRelease() {
        currentState.cancelSprintRelease();
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        // Remove the current product owner from the observers list.
        removeObserver(this.project.getProductOwner());

        // Register the new product owner as an observer.
        registerObserver(project.getProductOwner());

        this.project = project;
    }

    public String getSprintName() {
        return sprintName;
    }

    public void setSprintName(String sprintName) {
        this.sprintName = sprintName;
    }

    public String getSprintGoal() {
        return sprintGoal;
    }

    public void setSprintGoal(String sprintGoal) {
        this.sprintGoal = sprintGoal;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public SprintBacklog getSprintBacklog() {
        return sprintBacklog;
    }

    public void setSprintBacklog(SprintBacklog sprintBacklog) {
        this.sprintBacklog = sprintBacklog;
    }

    public ScrumMaster getScrumMaster() {
        return scrumMaster;
    }

    public void setScrumMaster(ScrumMaster scrumMaster) {
        // Remove the current scrum master from the observers list.
        removeObserver(this.scrumMaster);

        // Register the new scrum master as an observer.
        registerObserver(scrumMaster);

        // Remove the current scrum master from all product backlog items observers lists.
        // Register the new scrum master as an observer to all product backlog items.
        for (ProductBacklogItem productBacklogItem : this.sprintBacklog.getProductBacklogItems()) {
            productBacklogItem.removeObserver(this.scrumMaster);
            productBacklogItem.registerObserver(scrumMaster);
        }

        this.scrumMaster = scrumMaster;
    }

    public List<ScrumTeamMember> getScrumTeamMembers() {
        return scrumTeamMembers;
    }

    public ScrumTeamMember getScrumTeamMember(int i) {
        return scrumTeamMembers.get(i);
    }

    public void add(ScrumTeamMember scrumTeamMember) {
        // Register the new scrum team member as an observer.
        registerObserver(scrumTeamMember);

        scrumTeamMembers.add(scrumTeamMember);
    }

    public void removeScrumTeamMember(int i) {
        // Remove the scrum team member from the observers list.
        removeObserver(scrumTeamMembers.get(i));

        scrumTeamMembers.remove(i);
    }

    public DevelopmentPipeline getDevelopmentPipeline() {
        return developmentPipeline;
    }

    public void setDevelopmentPipeline(DevelopmentPipeline developmentPipeline) {
        this.developmentPipeline = developmentPipeline;
    }

    public SprintState getCreatedState() {
        return createdState;
    }

    public SprintState getInProgressState() {
        return inProgressState;
    }

    public SprintState getFinishedState() {
        return finishedState;
    }

    public SprintState getReviewState() {
        return reviewState;
    }

    public SprintState getFailedReleaseState() {
        return failedReleaseState;
    }

    public SprintState getCancelledReleaseState() {
        return cancelledReleaseState;
    }

    public SprintState getClosedState() {
        return closedState;
    }

    public SprintState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(SprintState state) {
        currentState = state;
    }

    @Override
    public void notifyObservers(String message) {
        for (Observer observer : observers) {
            observer.update(this, message);
        }
    }

    public void export(boolean includeHeader, boolean includeFooter) {
        sprintExportStrategy.export(this, includeHeader, includeFooter);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\nSprint{");
        sb.append("\nsprintName='").append(sprintName).append('\'');
        sb.append("\n, sprintGoal='").append(sprintGoal).append('\'');
        sb.append("\n, startDate=").append(startDate);
        sb.append("\n, endDate=").append(endDate);
        sb.append("\n, currentState=").append(currentState.getClass().getSimpleName());
        sb.append("\n}");
        return sb.toString();
    }
}
