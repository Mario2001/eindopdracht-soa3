package domain.devops;

public class Comment {
    private String message;
    private ScrumTeamMember scrumTeamMember;

    public Comment(String message, ScrumTeamMember scrumTeamMember) {
        this.message = message;
        this.scrumTeamMember = scrumTeamMember;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ScrumTeamMember getScrumTeamMember() {
        return scrumTeamMember;
    }

    public void setScrumTeamMember(ScrumTeamMember scrumTeamMember) {
        this.scrumTeamMember = scrumTeamMember;
    }
}
