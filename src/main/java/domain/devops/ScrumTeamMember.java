package domain.devops;

import adapter.sender.MessageSender;
import observer.Observer;
import observer.Subject;
import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.List;

public abstract class ScrumTeamMember implements Observer {
    private String name;
    private String email;
    private String phoneNumber;
    private List<ProductBacklogItem> productBacklogItems;
    private List<String> messages;
    private MessageSender messageSender;

    public ScrumTeamMember(String name, String email, String phoneNumber, MessageSender messageSender) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.productBacklogItems = new ArrayList<>();
        this.messages = new ArrayList<>();
        this.messageSender = messageSender;
    }

    public int getTotalStoryPoints() {
        int totalStoryPoints = 0;

        for (ProductBacklogItem productBacklogItem : productBacklogItems) {
            totalStoryPoints += productBacklogItem.getStoryPoints();
        }

        return totalStoryPoints;
    }

    public List<ProductBacklogItem> getProductBacklogItems() {
        return productBacklogItems;
    }

    public ProductBacklogItem getProductBacklogItem(int i) {
        return productBacklogItems.get(i);
    }

    public void add(ProductBacklogItem productBacklogItem) {
        productBacklogItems.add(productBacklogItem);
    }

    public void removeProductBacklogItem(int i) {
        productBacklogItems.remove(i);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<String> getMessages() {
        return messages;
    }

    public String getMessage(int i) {
        return messages.get(i);
    }

    @Override
    public void update(Subject subject, String message) {
        // Notification consists of scrum team member name, subject class name and message
        String notification = getName() +", "+ subject.getClass().getSimpleName() +", "+ message;
        Logger.info(notification);
        messages.add(notification);

        messageSender.send(notification);
    }
}
