package domain.devops;

public class Project {
    private String companyName;
    private String projectName;
    private double version;
    private ProductBacklog productBacklog;
    private ProductOwner productOwner;

    public Project(String companyName, String projectName, double version, ProductBacklog productBacklog, ProductOwner productOwner) {
        this.companyName = companyName;
        this.projectName = projectName;
        this.version = version;
        this.productBacklog = productBacklog;
        this.productOwner = productOwner;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public double getVersion() {
        return version;
    }

    public void setVersion(double version) {
        this.version = version;
    }

    public ProductBacklog getProductBacklog() {
        return productBacklog;
    }

    public void setProductBacklog(ProductBacklog productBacklog) {
        this.productBacklog = productBacklog;
    }

    public ProductOwner getProductOwner() {
        return productOwner;
    }

    public void setProductOwner(ProductOwner productOwner) {
        this.productOwner = productOwner;
    }
}
