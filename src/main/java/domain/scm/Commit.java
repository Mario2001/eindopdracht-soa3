package domain.scm;

import composite.directorystructure.DirectoryStructureComponent;

import java.time.LocalDateTime;

public class Commit {
    private String message;
    private LocalDateTime date;
    private VersionControlUser versionControlUser;
    private DirectoryStructureComponent directoryStructureComponent;

    public Commit(String message, LocalDateTime date, VersionControlUser versionControlUser, DirectoryStructureComponent directoryStructureComponent) {
        this.message = message;
        this.date = date;
        this.versionControlUser = versionControlUser;
        this.directoryStructureComponent = directoryStructureComponent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public VersionControlUser getVersionControlUser() {
        return versionControlUser;
    }

    public void setVersionControlUser(VersionControlUser versionControlUser) {
        this.versionControlUser = versionControlUser;
    }

    public DirectoryStructureComponent getDirectoryStructureComponent() {
        return directoryStructureComponent;
    }

    public void setDirectoryStructureComponent(DirectoryStructureComponent directoryStructureComponent) {
        this.directoryStructureComponent = directoryStructureComponent;
    }
}
