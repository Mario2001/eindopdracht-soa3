package domain.scm;

import java.util.ArrayList;
import java.util.List;

public class Branch {
    private String name;
    private List<Commit> commits;

    public Branch(String name) {
        this.name = name;
        commits = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Commit> getCommits() {
        return commits;
    }

    public Commit getCommit(int i) {
        return commits.get(i);
    }

    public void add(Commit commit) {
        commits.add(commit);
    }

    public void removeCommit(int i) {
        commits.remove(i);
    }
}
