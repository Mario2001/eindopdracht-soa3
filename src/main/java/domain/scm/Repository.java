package domain.scm;

import java.util.ArrayList;
import java.util.List;

public class Repository {
    private String name;
    private List<Branch> branches;

    public Repository(String name) {
        this.name = name;
        this.branches = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public Branch getBranch(int i) {
        return branches.get(i);
    }

    public void add(Branch branch) {
        branches.add(branch);
    }

    public void removeBranch(int i) {
        branches.remove(i);
    }
}
