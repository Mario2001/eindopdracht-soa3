package factory.pipeline;

import template.pipeline.DevelopmentPipeline;
import template.pipeline.TestPipeline;

public class TestPipelineCreator extends DevelopmentPipelineCreator {
    @Override
    public DevelopmentPipeline createDevelopmentPipeline() {
        return new TestPipeline();
    }
}
