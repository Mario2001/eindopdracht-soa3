package factory.pipeline;

import template.pipeline.DevelopmentPipeline;
import template.pipeline.UnitTestPipeline;

public class UnitTestPipelineCreator extends DevelopmentPipelineCreator {
    @Override
    public DevelopmentPipeline createDevelopmentPipeline() {
        return new UnitTestPipeline();
    }
}
