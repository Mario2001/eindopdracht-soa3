package factory.pipeline;

import template.pipeline.DevelopmentPipeline;

public abstract class DevelopmentPipelineCreator {
    public abstract DevelopmentPipeline createDevelopmentPipeline();
}
