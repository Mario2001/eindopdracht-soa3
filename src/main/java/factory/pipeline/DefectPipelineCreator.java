package factory.pipeline;

import template.pipeline.DefectPipeline;
import template.pipeline.DevelopmentPipeline;

public class DefectPipelineCreator extends DevelopmentPipelineCreator {
    @Override
    public DevelopmentPipeline createDevelopmentPipeline() {
        return new DefectPipeline();
    }
}
