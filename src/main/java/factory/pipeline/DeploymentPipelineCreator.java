package factory.pipeline;

import template.pipeline.DeploymentPipeline;
import template.pipeline.DevelopmentPipeline;

public class DeploymentPipelineCreator extends DevelopmentPipelineCreator {
    @Override
    public DevelopmentPipeline createDevelopmentPipeline() {
        return new DeploymentPipeline();
    }
}
