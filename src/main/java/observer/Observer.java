package observer;

public interface Observer {
    void update(Subject subject, String message);
}
