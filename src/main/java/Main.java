import adapter.sender.EmailAdapter;
import adapter.sender.MessageSender;
import domain.devops.*;
import factory.pipeline.DevelopmentPipelineCreator;
import factory.pipeline.TestPipelineCreator;
import library.EmailSender;
import org.tinylog.Logger;
import strategy.export.SprintExportType;
import template.pipeline.DevelopmentPipeline;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        DevelopmentPipelineCreator developmentPipelineCreator = new TestPipelineCreator();

        ProductBacklog productBacklog = new ProductBacklog();
        SprintBacklog sprint1Backlog = new SprintBacklog(null);

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        List<ScrumTeamMember> scrumTeamMembers = new ArrayList<>();
        ScrumTeamMember developer = new Developer("Iejoor", "iejoor@disney.com", "+31612345678", messageSender);
        scrumTeamMembers.add(developer);
        ScrumTeamMember tester = new Tester("Uil", "uil@disney.com", "+31612345678", messageSender);
        scrumTeamMembers.add(tester);

        Project project = new Project("Avans", "DevOps", 1, productBacklog, productOwner);

        Sprint sprint1 = new RegularSprint(project,"Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), sprint1Backlog, scrumMaster, scrumTeamMembers, SprintExportType.PDF);
        sprint1Backlog.setSprint(sprint1);
        DevelopmentPipeline developmentPipeline = developmentPipelineCreator.createDevelopmentPipeline();
        sprint1.setDevelopmentPipeline(developmentPipeline);

        ProductBacklogItem productBacklogItem1 = new ProductBacklogItem("Honing zoeken", "Een pot met honing zoeken", 2);
        ProductBacklogItem productBacklogItem2 = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 4);
        ProductBacklogItem productBacklogItem3 = new ProductBacklogItem("Blaadjes vangen", "Vallende blaadjes van een boom vangen", 2);
        ProductBacklogItem productBacklogItem4 = new ProductBacklogItem("Ballon maken", "Een ballon maken", 5);
        productBacklogItem1.setScrumTeamMember(developer);
        productBacklogItem2.setScrumTeamMember(developer);
        productBacklogItem4.setScrumTeamMember(tester);
        productBacklogItem3.setScrumTeamMember(tester);
        sprint1Backlog.add(productBacklogItem1);
        sprint1Backlog.add(productBacklogItem2);
        sprint1Backlog.add(productBacklogItem3);
        sprint1Backlog.add(productBacklogItem4);

        sprint1.export(true, true);

        Logger.info("De product owner is: " + project.getProductOwner().getName());

        sprint1.startSprint();
        sprint1.finishSprint();
        sprint1.reviewSprint();
        sprint1.releaseSprint();
        Logger.info(sprint1.getCurrentState());
    }
}
