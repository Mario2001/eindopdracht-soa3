package library;

import org.tinylog.Logger;

public class SlackSender {

    public void sendSlackMessage(String message) {
        Logger.info(message);
    }
}
