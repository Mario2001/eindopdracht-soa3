package template.pipeline;

import org.tinylog.Logger;

public class TestPipeline extends DevelopmentPipeline {
    @Override
    protected void installPackages() {
        Logger.info("Pipeline: install the packages.");
    }

    @Override
    protected void build() {
        Logger.info("Pipeline: build the source code.");
    }

    @Override
    protected void test() {
        Logger.info("Pipeline: run the tests.");
    }

    @Override
    protected void prepareAnalysis() {
        Logger.info("Pipeline: prepare the code analysis.");
    }

    @Override
    protected void executeAnalysis() {
        Logger.info("Pipeline: run the code analysis.");
    }

    @Override
    protected void reportAnalysis() {
        Logger.info("Pipeline: report the code analysis.");
    }

    @Override
    protected void executeUtilities() {
        Logger.info("Pipeline: run a script.");
    }
}
