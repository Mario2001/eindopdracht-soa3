package template.pipeline;

import org.tinylog.Logger;

public class DefectPipeline extends DevelopmentPipeline {
    @Override
    protected void installPackages() {
        Logger.info("Pipeline: install the packages.");
    }

    @Override
    protected void build() {
        Logger.info("Pipeline: build the source code.");
    }

    @Override
    protected void test() {
        Logger.info("Pipeline: run the tests.");
        throw new NullPointerException();
    }

    @Override
    protected void executeAnalysis() {
        Logger.info("Pipeline: execute the code analysis.");
    }
}
