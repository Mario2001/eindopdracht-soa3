package template.pipeline;

import org.tinylog.Logger;

public class UnitTestPipeline extends DevelopmentPipeline {
    private boolean hasReachedInstallPackagesStage = false;
    private boolean hasReachedBuildStage = false;
    private boolean hasReachedTestStage = false;
    private boolean hasReachedPrepareAnalysisStage = false;
    private boolean hasReachedExecuteAnalysisStage = false;
    private boolean hasReachedReportAnalysisStage = false;
    private boolean hasReachedDeployStage = false;
    private boolean hasReachedExecuteUtilitiesStage = false;

    @Override
    protected void installPackages() {
        Logger.info("Pipeline: install the packages.");
        hasReachedInstallPackagesStage = true;
    }

    @Override
    protected void build() {
        Logger.info("Pipeline: build the source code.");
        hasReachedBuildStage = true;
    }

    @Override
    protected void test() {
        Logger.info("Pipeline: run the tests.");
        hasReachedTestStage = true;
    }

    @Override
    protected void prepareAnalysis() {
        Logger.info("Pipeline: prepare the code analysis.");
        hasReachedPrepareAnalysisStage = true;
    }

    @Override
    protected void executeAnalysis() {
        Logger.info("Pipeline: run the code analysis.");
        hasReachedExecuteAnalysisStage = true;
    }

    @Override
    protected void reportAnalysis() {
        Logger.info("Pipeline: report the code analysis.");
        hasReachedReportAnalysisStage = true;
    }

    @Override
    protected void deploy() {
        Logger.info("Pipeline: deploy the application.");
        hasReachedDeployStage = true;
    }

    @Override
    protected void executeUtilities() {
        Logger.info("Pipeline: run a script.");
        hasReachedExecuteUtilitiesStage = true;
    }

    public boolean hasReachedInstallPackagesStage() {
        return hasReachedInstallPackagesStage;
    }

    public boolean hasReachedBuildStage() {
        return hasReachedBuildStage;
    }

    public boolean hasReachedTestStage() {
        return hasReachedTestStage;
    }

    public boolean hasReachedPrepareAnalysisStage() {
        return hasReachedPrepareAnalysisStage;
    }

    public boolean hasReachedExecuteAnalysisStage() {
        return hasReachedExecuteAnalysisStage;
    }

    public boolean hasReachedReportAnalysisStage() {
        return hasReachedReportAnalysisStage;
    }

    public boolean hasReachedDeployStage() {
        return hasReachedDeployStage;
    }

    public boolean hasReachedExecuteUtilitiesStage() {
        return hasReachedExecuteUtilitiesStage;
    }
}
