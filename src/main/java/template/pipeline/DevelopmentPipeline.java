package template.pipeline;

import org.tinylog.Logger;

public abstract class DevelopmentPipeline {
    public final void execute() {
        retrieveSources();
        installPackages();
        build();
        test();
        analyse();
        deploy();
        executeUtilities();
    }

    private void retrieveSources() {
        Logger.info("Pipeline: retrieve the sources.");
    }

    protected abstract void installPackages();

    protected abstract void build();

    protected abstract void test();

    private void analyse() {
        prepareAnalysis();
        executeAnalysis();
        reportAnalysis();
    }

    protected void prepareAnalysis() {
        Logger.info("Pipeline: no analysis preparation.");
    }

    protected abstract void executeAnalysis();

    protected void reportAnalysis() {
        Logger.info("Pipeline: no analysis reporting.");
    }

    protected void deploy() {
        Logger.info("Pipeline: no deployment.");
    }

    protected void executeUtilities() {
        Logger.info("Pipeline: no utilities execution.");
    }
}
