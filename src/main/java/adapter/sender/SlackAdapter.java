package adapter.sender;

import library.SlackSender;

public class SlackAdapter implements MessageSender {

    private SlackSender slackSender;

    public SlackAdapter(SlackSender slackSender) {
        this.slackSender = slackSender;
    }

    @Override
    public void send(String message) {
        slackSender.sendSlackMessage(message);
    }
}
