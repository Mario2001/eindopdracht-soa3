package adapter.sender;

import library.EmailSender;

public class EmailAdapter implements MessageSender {

    private EmailSender emailSender;

    public EmailAdapter(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Override
    public void send(String message) {
        emailSender.sendEmail(message);
    }
}
