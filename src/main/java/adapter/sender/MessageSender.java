package adapter.sender;

public interface MessageSender {
    void send(String message);
}
