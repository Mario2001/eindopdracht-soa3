package adapter.sender;

import library.SMSSender;

public class SMSAdapter implements MessageSender {

    private SMSSender smsSender;

    public SMSAdapter(SMSSender smsSender) {
        this.smsSender = smsSender;
    }

    @Override
    public void send(String message) {
        smsSender.sendSMS(message);
    }
}
