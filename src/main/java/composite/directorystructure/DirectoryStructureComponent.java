package composite.directorystructure;

public abstract class DirectoryStructureComponent {
    public void add(DirectoryStructureComponent directoryStructureComponent) {
        throw new UnsupportedOperationException("Unable to add DirectoryStructureComponent.");
    }

    public void remove(DirectoryStructureComponent directoryStructureComponent) {
        throw new UnsupportedOperationException("Unable to remove DirectoryStructureComponent");
    }

    public DirectoryStructureComponent getChild(int i) {
        throw new UnsupportedOperationException("Unable to retrieve DirectoryStructureComponent");
    }

    public abstract String getName();

    public abstract double getTotalFileSize();
}
