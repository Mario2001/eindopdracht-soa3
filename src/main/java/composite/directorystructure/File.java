package composite.directorystructure;

public class File extends DirectoryStructureComponent {
    private String fileName;
    private String fileExtension;
    private double fileSize;

    public File(String fileName, String fileExtension, double fileSize) {
        this.fileName = fileName;
        this.fileExtension = fileExtension;
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public double getFileSize() {
        return fileSize;
    }

    public void setFileSize(double fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public String getName() {
        return getFileName();
    }

    @Override
    public double getTotalFileSize() {
        return getFileSize();
    }
}
