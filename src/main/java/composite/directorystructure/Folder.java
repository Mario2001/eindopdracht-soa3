package composite.directorystructure;

import java.util.ArrayList;
import java.util.List;

public class Folder extends DirectoryStructureComponent {
    private String folderName;
    private List<DirectoryStructureComponent> directoryStructureComponents;

    public Folder(String folderName) {
        this.folderName = folderName;
        directoryStructureComponents = new ArrayList<>();
    }

    @Override
    public void add(DirectoryStructureComponent directoryStructureComponent) {
        directoryStructureComponents.add(directoryStructureComponent);
    }

    @Override
    public void remove(DirectoryStructureComponent directoryStructureComponent) {
        directoryStructureComponents.remove(directoryStructureComponent);
    }

    @Override
    public DirectoryStructureComponent getChild(int i) {
        return directoryStructureComponents.get(i);
    }

    @Override
    public String getName() {
        return getFolderName();
    }

    @Override
    public double getTotalFileSize() {
        double totalFileSize = 0;

        for (DirectoryStructureComponent directoryStructureComponent : directoryStructureComponents) {
            totalFileSize += directoryStructureComponent.getTotalFileSize();
        }

        return totalFileSize;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }
}
