package state.product_backlog_item;

import domain.devops.ProductBacklogItem;
import domain.devops.ScrumTeamMember;
import org.tinylog.Logger;

public class TestingState implements ProductBacklogItemState {
    private ProductBacklogItem productBacklogItem;

    public TestingState(ProductBacklogItem productBacklogItem) {
        this.productBacklogItem = productBacklogItem;
    }

    @Override
    public void setToTodo() {
        Logger.info("Product Backlog Item: Set to TODO");
        productBacklogItem.getSprintBacklog().getSprint().getScrumMaster().update(productBacklogItem, "Item rejected from testing back to todo");
        productBacklogItem.setCurrentState(productBacklogItem.getToDoState());
    }

    @Override
    public void setToDoing(ScrumTeamMember scrumTeamMember) {
        Logger.error("Product Backlog Item: Error - cannot set to Doing");
    }

    @Override
    public void setToReadyForTesting() {
        Logger.error("Product Backlog Item: Error - cannot set to READY FOR TESTING");
    }

    @Override
    public void setToTesting() {
        Logger.error("Product Backlog Item: Error - cannot set to TESTING");
    }

    @Override
    public void setToTested() {
        Logger.info("Product Backlog Item: Set to TESTED");
        productBacklogItem.setCurrentState(productBacklogItem.getTestedState());
    }

    @Override
    public void setToDone() {
        Logger.error("Product Backlog Item: Error - cannot set to DONE");
    }
}
