package state.product_backlog_item;

import domain.devops.ProductBacklogItem;
import domain.devops.ScrumTeamMember;
import domain.devops.Tester;
import org.tinylog.Logger;

public class DoingState implements ProductBacklogItemState {
    private ProductBacklogItem productBacklogItem;

    public DoingState(ProductBacklogItem productBacklogItem) {
        this.productBacklogItem = productBacklogItem;
    }

    @Override
    public void setToTodo() {
        Logger.error("Product Backlog Item: Error - cannot set to TODO");
    }

    @Override
    public void setToDoing(ScrumTeamMember scrumTeamMember) {
        Logger.error("Product Backlog Item: Error - cannot set to Doing");
    }

    @Override
    public void setToReadyForTesting() {
        Logger.info("Product Backlog Item: Set to READY FOR TESTING");

        for (ScrumTeamMember scrumTeamMember : productBacklogItem.getSprintBacklog().getSprint().getScrumTeamMembers()) {
            if (scrumTeamMember instanceof Tester) {
                scrumTeamMember.update(productBacklogItem, "New item for testing");
            }
        }

        productBacklogItem.setCurrentState(productBacklogItem.getReadyForTestingState());
    }

    @Override
    public void setToTesting() {
        Logger.error("Product Backlog Item: Error - cannot set to TESTING");
    }

    @Override
    public void setToTested() {
        Logger.error("Product Backlog Item: Error - cannot set to TESTED");
    }

    @Override
    public void setToDone() {
        Logger.error("Product Backlog Item: Error - cannot set to DONE");
    }
}
