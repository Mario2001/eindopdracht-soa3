package state.product_backlog_item;

import domain.devops.ProductBacklogItem;
import domain.devops.ScrumTeamMember;
import org.tinylog.Logger;

public class ToDoState implements ProductBacklogItemState {
    private ProductBacklogItem productBacklogItem;

    public ToDoState(ProductBacklogItem productBacklogItem) {
        this.productBacklogItem = productBacklogItem;
    }

    @Override
    public void setToTodo() {
        Logger.error("Product Backlog Item: Error - cannot set to TODO");
    }

    @Override
    public void setToDoing(ScrumTeamMember scrumTeamMember) {
        // Check if STM has 0 item with Sprint Backlog item (loop)
        for (ProductBacklogItem item : scrumTeamMember.getProductBacklogItems()) {
            // Check if current item is in to do
            if (item.getCurrentState() instanceof DoingState) {
                Logger.error("Product Backlog Item: ERROR - ScrumTeamMember is already working on a ProductBacklogItem");
                return;
            }
        }

        Logger.info("Product Backlog Item: Set to Doing");
        productBacklogItem.setScrumTeamMember(scrumTeamMember);
        productBacklogItem.setCurrentState(productBacklogItem.getDoingState());
    }

    @Override
    public void setToReadyForTesting() {
        Logger.error("Product Backlog Item: Error - cannot set to READY FOR TESTING");
    }

    @Override
    public void setToTesting() {
        Logger.error("Product Backlog Item: Error - cannot set to TESTING");
    }

    @Override
    public void setToTested() {
        Logger.error("Product Backlog Item: Error - cannot set to TESTED");
    }

    @Override
    public void setToDone() {
        Logger.error("Product Backlog Item: Error - cannot set to DONE");
    }
}
