package state.product_backlog_item;

import domain.devops.ScrumTeamMember;

public interface ProductBacklogItemState {
    void setToTodo();
    void setToDoing(ScrumTeamMember scrumTeamMember);
    void setToReadyForTesting();
    void setToTesting();
    void setToTested();
    void setToDone();
}
