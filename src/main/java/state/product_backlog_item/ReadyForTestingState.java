package state.product_backlog_item;

import domain.devops.ProductBacklogItem;
import domain.devops.ScrumTeamMember;
import org.tinylog.Logger;

public class ReadyForTestingState implements ProductBacklogItemState {
    private ProductBacklogItem productBacklogItem;

    public ReadyForTestingState(ProductBacklogItem productBacklogItem) {
        this.productBacklogItem = productBacklogItem;
    }

    @Override
    public void setToTodo() {
        Logger.error("Product Backlog Item: Error - cannot set to TODO");
    }

    @Override
    public void setToDoing(ScrumTeamMember scrumTeamMember) {
        Logger.error("Product Backlog Item: Error - cannot set to Doing");
    }

    @Override
    public void setToReadyForTesting() {
        Logger.error("Product Backlog Item: Error - cannot set to READY FOR TESTING");
    }

    @Override
    public void setToTesting() {
        Logger.info("Product Backlog Item: Set to TESTING");
        productBacklogItem.setCurrentState(productBacklogItem.getTestingState());
    }

    @Override
    public void setToTested() {
        Logger.error("Product Backlog Item: Error - cannot set to TESTED");
    }

    @Override
    public void setToDone() {
        Logger.error("Product Backlog Item: Error - cannot set to DONE");
    }
}
