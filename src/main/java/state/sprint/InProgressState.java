package state.sprint;

import domain.devops.Sprint;
import org.tinylog.Logger;

public class InProgressState implements SprintState {
    private Sprint sprint;

    public InProgressState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void startSprint() {
        Logger.error("Error: cannot start the in progress sprint.");
    }

    @Override
    public void finishSprint() {
        Logger.info("Finish the in progress sprint.");
        sprint.setCurrentState(sprint.getFinishedState());
    }

    @Override
    public void reviewSprint() {
        Logger.error("Error: cannot review the in progress sprint.");
    }

    @Override
    public void releaseSprint() {
        Logger.error("Error: cannot release the in progress sprint.");
    }

    @Override
    public void cancelSprintRelease() {
        Logger.error("Error: cannot cancel the sprint release from the in progress sprint.");
    }
}
