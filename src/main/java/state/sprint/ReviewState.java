package state.sprint;

import domain.devops.Sprint;
import org.tinylog.Logger;

public class ReviewState implements SprintState {
    private Sprint sprint;

    public ReviewState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void startSprint() {
        Logger.error("Error: cannot start the reviewed sprint.");
    }

    @Override
    public void finishSprint() {
        Logger.error("Error: cannot finish the reviewed sprint.");
    }

    @Override
    public void reviewSprint() {
        Logger.error("Error: cannot review the reviewed sprint.");
    }

    @Override
    public void releaseSprint() {
        Logger.info("Start the development pipeline of the reviewed sprint.");

        try {
            sprint.getDevelopmentPipeline().execute();

            String message = "Pipeline success for sprint: " + sprint.getSprintName();
            sprint.getScrumMaster().update(sprint, message);
            sprint.getProject().getProductOwner().update(sprint, message);

            sprint.setCurrentState(sprint.getClosedState());
        } catch (Exception e) {
            Logger.error(e);

            sprint.getScrumMaster().update(sprint, "Pipeline failed for sprint: " + sprint.getSprintName());

            sprint.setCurrentState(sprint.getFailedReleaseState());
        }
    }

    @Override
    public void cancelSprintRelease() {
        Logger.info("Cancel the sprint release.");

        String message = "Sprint: " + sprint.getSprintName() +" Sprint release cancelled";
        sprint.getScrumMaster().update(sprint, message);
        sprint.getProject().getProductOwner().update(sprint, message);

        sprint.setCurrentState(sprint.getCancelledReleaseState());
    }
}
