package state.sprint;

public interface SprintState {
    void startSprint();
    void finishSprint();
    void reviewSprint();
    void releaseSprint();
    void cancelSprintRelease();
}
