package state.sprint;

import domain.devops.Sprint;
import org.tinylog.Logger;

public class FailedReleaseState implements SprintState {
    private Sprint sprint;

    public FailedReleaseState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void startSprint() {
        Logger.error("Error: cannot start the failed sprint release.");
    }

    @Override
    public void finishSprint() {
        Logger.error("Error: cannot finish the failed sprint release.");
    }

    @Override
    public void reviewSprint() {
        Logger.error("Error: cannot review the failed sprint release.");
    }

    @Override
    public void releaseSprint() {
        Logger.info("Start the development pipeline of the failed sprint release.");

        try {
            sprint.getDevelopmentPipeline().execute();

            String message = "Pipeline success for sprint: " + sprint.getSprintName();
            sprint.getScrumMaster().update(sprint, message);
            sprint.getProject().getProductOwner().update(sprint, message);

            sprint.setCurrentState(sprint.getClosedState());
        } catch (Exception e) {
            Logger.error(e);

            sprint.getScrumMaster().update(sprint, "Pipeline failed for sprint: " + sprint.getSprintName());

            sprint.setCurrentState(sprint.getFailedReleaseState());
        }
    }

    @Override
    public void cancelSprintRelease() {
        Logger.error("Cancel the sprint release.");

        String message = "Sprint: " + sprint.getSprintName() +" Sprint release cancelled";
        sprint.getScrumMaster().update(sprint, message);
        sprint.getProject().getProductOwner().update(sprint, message);

        sprint.setCurrentState(sprint.getCancelledReleaseState());
    }
}
