package state.sprint;

import domain.devops.Sprint;
import org.tinylog.Logger;

public class ClosedState implements SprintState {
    private Sprint sprint;

    public ClosedState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void startSprint() {
        Logger.error("Error: cannot start the closed sprint.");
    }

    @Override
    public void finishSprint() {
        Logger.error("Error: cannot finish the closed sprint.");
    }

    @Override
    public void reviewSprint() {
        Logger.error("Error: cannot review the closed sprint.");
    }

    @Override
    public void releaseSprint() {
        Logger.error("Error: cannot release the closed sprint.");
    }

    @Override
    public void cancelSprintRelease() {
        Logger.error("Error: cannot cancel the sprint release from the closed sprint.");
    }
}
