package state.sprint;

import domain.devops.Sprint;
import org.tinylog.Logger;

public class CancelledReleaseState implements SprintState {
    private Sprint sprint;

    public CancelledReleaseState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void startSprint() {
        Logger.error("Error: cannot start the cancelled release.");
    }

    @Override
    public void finishSprint() {
        Logger.error("Error: cannot finish the cancelled release.");
    }

    @Override
    public void reviewSprint() {
        Logger.error("Error: cannot review the cancelled release.");
    }

    @Override
    public void releaseSprint() {
        Logger.error("Error: cannot release the cancelled release.");
    }

    @Override
    public void cancelSprintRelease() {
        Logger.error("Error: cannot cancel the sprint release from the cancelled sprint release.");
    }
}
