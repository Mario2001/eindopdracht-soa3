package state.sprint;

import domain.devops.Sprint;
import org.tinylog.Logger;

public class FinishedState implements SprintState {
    private Sprint sprint;

    public FinishedState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void startSprint() {
        Logger.error("Error: cannot start the finished sprint.");
    }

    @Override
    public void finishSprint() {
        Logger.error("Error: cannot finish the finished sprint.");
    }

    @Override
    public void reviewSprint() {
        Logger.info("Review the finished sprint.");
        sprint.setCurrentState(sprint.getReviewState());
    }

    @Override
    public void releaseSprint() {
        Logger.info("Start the development pipeline of the finished sprint.");

        try {
            sprint.getDevelopmentPipeline().execute();

            String message = "Pipeline success for sprint: " + sprint.getSprintName();
            sprint.getScrumMaster().update(sprint, message);
            sprint.getProject().getProductOwner().update(sprint, message);

            sprint.setCurrentState(sprint.getClosedState());
        } catch (Exception e) {
            Logger.error(e);

            sprint.getScrumMaster().update(sprint, "Pipeline failed for sprint: " + sprint.getSprintName());

            sprint.setCurrentState(sprint.getFailedReleaseState());
        }
    }

    @Override
    public void cancelSprintRelease() {
        Logger.error("Error: cannot cancel the sprint release from the finished sprint.");
    }
}
