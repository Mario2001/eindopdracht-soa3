package state.sprint;

import domain.devops.Sprint;
import org.tinylog.Logger;

public class CreatedState implements SprintState {
    private Sprint sprint;

    public CreatedState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void startSprint() {
        Logger.info("Start the created sprint.");
        sprint.setCurrentState(sprint.getInProgressState());
    }

    @Override
    public void finishSprint() {
        Logger.error("Error: cannot finish the created sprint.");
    }

    @Override
    public void reviewSprint() {
        Logger.error("Error: cannot review the created sprint.");
    }

    @Override
    public void releaseSprint() {
        Logger.error("Error: cannot release the created sprint.");
    }

    @Override
    public void cancelSprintRelease() {
        Logger.error("Error: cannot cancel the sprint release from the created sprint.");
    }
}
