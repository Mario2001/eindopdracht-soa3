package strategy.export;

public enum SprintExportType {
    PDF,
    PNG
}
