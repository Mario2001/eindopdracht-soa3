package strategy.export;

import domain.devops.Sprint;

public interface SprintExportStrategy {
    void export(Sprint sprint, boolean includeHeader, boolean includeFooter);
}
