package strategy.export;

import domain.devops.ScrumTeamMember;
import domain.devops.Sprint;
import org.tinylog.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;

public class SprintPDFExportStrategy implements SprintExportStrategy {
    @Override
    public void export(Sprint sprint, boolean includeHeader, boolean includeFooter) {
        File file = new File("./exports/" + sprint.getProject().getProjectName() + "-" + sprint.getSprintName() + "-PDF.txt");
        File directory = new File("./exports");
        if (!directory.exists()) {
            directory.mkdir();
        }

        try (FileWriter fileWriter = new FileWriter(file)) {
            if (includeHeader) {
                fileWriter.append("---------- HEADER ----------\n\n");
            }

            fileWriter.append("Samenvatting huidige sprint:");
            fileWriter.append(sprint.toString());

            long sprintDuration = Duration.between(sprint.getStartDate(), sprint.getEndDate()).toDays();
            fileWriter.append("\nSprintduur: ").append(Long.toString(sprintDuration)).append(" dagen");

            fileWriter.append("\n\nOverzicht teamleden:\n");
            for (ScrumTeamMember scrumTeamMember: sprint.getScrumTeamMembers()) {
                fileWriter.append(scrumTeamMember.getName());
                fileWriter.append(", aantal punten: ").append(Integer.toString(scrumTeamMember.getTotalStoryPoints())).append("\n");
            }

            fileWriter.append("\nBurndownchart: ...");

            if (includeFooter) {
                fileWriter.append("\n\n---------- FOOTER ----------");
            }
        } catch (IOException e) {
            Logger.error(e);
        }
    }
}
