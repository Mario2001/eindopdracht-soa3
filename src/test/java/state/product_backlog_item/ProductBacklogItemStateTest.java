package state.product_backlog_item;

import adapter.sender.EmailAdapter;
import adapter.sender.MessageSender;
import domain.devops.*;
import library.EmailSender;
import org.junit.jupiter.api.Test;
import strategy.export.SprintExportType;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ProductBacklogItemStateTest {
    @Test
    void scrumTeamMemberIsAbleToWorkOnAProductBacklogItem() {
        // Arrange
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", null);
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);

        // Act
        productBacklogItem.setToDoing(scrumTeamMember);

        // Assert
        assertEquals(DoingState.class, productBacklogItem.getCurrentState().getClass());
    }

    @Test
    void scrumTeamMemberAlreadyDoingAProductBacklogItem() {
        // Arrange
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", null);
        ProductBacklogItem productBacklogItem1 = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        productBacklogItem1.setToDoing(scrumTeamMember);
        ProductBacklogItem productBacklogItem2 = new ProductBacklogItem("Vlinders vangen", "Vlinders met een netje vangen", 3);
        scrumTeamMember.add(productBacklogItem1);
        scrumTeamMember.add(productBacklogItem2);

        // Act
        productBacklogItem2.setToDoing(scrumTeamMember);

        // Assert
        assertEquals(ToDoState.class, productBacklogItem2.getCurrentState().getClass());
    }

    @Test
    void toDo_Doing_ReadyForTesting_Testing_Tested_Done() {
        // Arrange
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, null, new ArrayList<>(), SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        productBacklogItem.setSprintBacklog(sprintBacklog);

        // Act
        productBacklogItem.setToDoing(scrumTeamMember);
        productBacklogItem.setToReadyForTesting();
        productBacklogItem.setToTesting();
        productBacklogItem.setToTested();
        productBacklogItem.setToDone();

        // Assert
        assertEquals(DoneState.class, productBacklogItem.getCurrentState().getClass());
    }

    @Test
    void toDo_Doing_ReadyForTesting_Testing_ToDo() {
        // Arrange
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        productBacklogItem.setSprintBacklog(sprintBacklog);

        // Act
        productBacklogItem.setToDoing(scrumTeamMember);
        productBacklogItem.setToReadyForTesting();
        productBacklogItem.setToTesting();
        productBacklogItem.setToToDo();

        // Assert
        assertEquals(ToDoState.class, productBacklogItem.getCurrentState().getClass());
    }

    @Test
    void toDo_Doing_ReadyForTesting_Testing_Tested_ReadyForTesting() {
        // Arrange
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, null, new ArrayList<>(), SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        productBacklogItem.setSprintBacklog(sprintBacklog);

        // Act
        productBacklogItem.setToDoing(scrumTeamMember);
        productBacklogItem.setToReadyForTesting();
        productBacklogItem.setToTesting();
        productBacklogItem.setToTested();
        productBacklogItem.setToReadyForTesting();

        // Assert
        assertEquals(ReadyForTestingState.class, productBacklogItem.getCurrentState().getClass());
    }

    @Test
    void toDo_Doing_ReadyForTesting_Testing_Tested_Done_ToDo() {
        // Arrange
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, null, new ArrayList<>(), SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        productBacklogItem.setSprintBacklog(sprintBacklog);

        // Act
        productBacklogItem.setToDoing(scrumTeamMember);
        productBacklogItem.setToReadyForTesting();
        productBacklogItem.setToTesting();
        productBacklogItem.setToTested();
        productBacklogItem.setToDone();
        productBacklogItem.setToToDo();

        // Assert
        assertEquals(ToDoState.class, productBacklogItem.getCurrentState().getClass());
    }
}