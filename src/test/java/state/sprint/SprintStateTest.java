package state.sprint;

import adapter.sender.EmailAdapter;
import adapter.sender.MessageSender;
import domain.devops.*;
import factory.pipeline.DefectPipelineCreator;
import factory.pipeline.DevelopmentPipelineCreator;
import factory.pipeline.TestPipelineCreator;
import library.EmailSender;
import org.junit.jupiter.api.Test;
import strategy.export.SprintExportType;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SprintStateTest {
    private static void sleep() throws InterruptedException {
        Thread.sleep(100);
    }

    @Test
    void created_InProgress_FinishedByTimer() throws InterruptedException {
        // Arrange
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().minusDays(1), null, null, new ArrayList<>(), SprintExportType.PDF);

        // Act
        sprint.startSprint();
        sleep();

        // Assert
        System.out.println(sprint.getCurrentState());
        assertEquals(FinishedState.class, sprint.getCurrentState().getClass());
    }

    @Test
    void created_InProgress_Finished_Closed() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new TestPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.releaseSprint();

        // Assert
        assertEquals(ClosedState.class, sprint.getCurrentState().getClass());
    }

    @Test
    void created_InProgress_Finished_FailedRelease_Closed() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.releaseSprint();

        assertEquals(FailedReleaseState.class, sprint.getCurrentState().getClass());

        developmentPipelineCreator = new TestPipelineCreator();
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        sprint.releaseSprint();

        // Assert
        assertEquals(ClosedState.class, sprint.getCurrentState().getClass());
    }

    @Test
    void created_InProgress_Finished_FailedRelease_FailedRelease_Closed() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project,"Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.releaseSprint();

        assertEquals(FailedReleaseState.class, sprint.getCurrentState().getClass());

        sprint.releaseSprint();

        assertEquals(FailedReleaseState.class, sprint.getCurrentState().getClass());

        developmentPipelineCreator = new TestPipelineCreator();
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        sprint.releaseSprint();

        // Assert
        assertEquals(ClosedState.class, sprint.getCurrentState().getClass());
    }

    @Test
    void created_InProgress_Finished_FailedRelease_CancelledRelease() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project,"Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.releaseSprint();

        assertEquals(FailedReleaseState.class, sprint.getCurrentState().getClass());

        sprint.cancelSprintRelease();

        // Assert
        assertEquals(CancelledReleaseState.class, sprint.getCurrentState().getClass());
    }

    @Test
    void created_InProgress_Finished_Review_Closed() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new TestPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project,"Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.reviewSprint();
        sprint.releaseSprint();

        // Assert
        assertEquals(ClosedState.class, sprint.getCurrentState().getClass());
    }

    @Test
    void created_InProgress_Finished_Review_FailedRelease_Closed() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project,"Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.reviewSprint();
        sprint.releaseSprint();

        assertEquals(FailedReleaseState.class, sprint.getCurrentState().getClass());

        developmentPipelineCreator = new TestPipelineCreator();
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        sprint.releaseSprint();

        // Assert
        assertEquals(ClosedState.class, sprint.getCurrentState().getClass());
    }

    @Test
    void created_InProgress_Finished_Review_FailedRelease_CancelledRelease() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.reviewSprint();
        sprint.releaseSprint();

        assertEquals(FailedReleaseState.class, sprint.getCurrentState().getClass());

        sprint.cancelSprintRelease();

        // Assert
        assertEquals(CancelledReleaseState.class, sprint.getCurrentState().getClass());
    }

    @Test
    void created_InProgress_Finished_Review_CancelledRelease() {
        // Arrange
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project,"Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.reviewSprint();
        sprint.cancelSprintRelease();

        // Assert
        assertEquals(CancelledReleaseState.class, sprint.getCurrentState().getClass());
    }

    @Test
    void canChangeTheSprintStateToClosedStateWhenTheDevelopmentPipelineHasExecuted() {
        // Arrange
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project,"Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.setCurrentState(sprint.getClosedState());

        // Assert
        assertEquals(ClosedState.class, sprint.getCurrentState().getClass());
    }

    @Test
    void canChangeTheSprintStateToFailedReleaseStateWhenTheDevelopmentPipelineHasExecuted() {
        // Arrange
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project,"Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.setCurrentState(sprint.getFailedReleaseState());

        // Assert
        assertEquals(FailedReleaseState.class, sprint.getCurrentState().getClass());
    }
}