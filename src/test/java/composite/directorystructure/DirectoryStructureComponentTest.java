package composite.directorystructure;

import domain.scm.Commit;
import domain.scm.VersionControlUser;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class DirectoryStructureComponentTest {
    @Test
    void totalFileSizeOfDirectoryStructureShouldEqual30() {
        // Arrange
        DirectoryStructureComponent folder1 = new Folder("Folder 1");
        DirectoryStructureComponent folder2 = new Folder("Folder 2");
        DirectoryStructureComponent folder3 = new Folder("Folder 3");

        folder1.add(folder2);
        folder1.add(folder3);

        DirectoryStructureComponent file1 = new File("File 1", ".txt", 10);
        DirectoryStructureComponent file2 = new File("File 2", ".txt", 10);
        DirectoryStructureComponent file3 = new File("File 3", ".txt", 10);

        folder1.add(file1);
        folder2.add(file2);
        folder3.add(file3);

        VersionControlUser versionControlUser = new VersionControlUser("Tijgetje", "tijgetje@disney.com");
        Commit commit = new Commit("Initial commit", LocalDateTime.now(), versionControlUser, folder1);

        // Act
        double totalFileSize = commit.getDirectoryStructureComponent().getTotalFileSize();

        // Assert
        assertEquals(30, totalFileSize);
    }

    @Test
    void requestingDirectoryStructureComponentShouldReturnCorrectFile() {
        // Arrange
        DirectoryStructureComponent folder1 = new Folder("Folder 1");
        DirectoryStructureComponent folder2 = new Folder("Folder 2");
        DirectoryStructureComponent folder3 = new Folder("Folder 3");

        folder1.add(folder2);
        folder1.add(folder3);

        DirectoryStructureComponent file1 = new File("File 1", ".txt", 10);
        DirectoryStructureComponent file2 = new File("File 2", ".txt", 10);
        DirectoryStructureComponent file3 = new File("File 3", ".txt", 10);

        folder1.add(file1);
        folder2.add(file2);
        folder3.add(file3);

        VersionControlUser versionControlUser = new VersionControlUser("Tijgetje", "tijgetje@disney.com");
        Commit commit = new Commit("Initial commit", LocalDateTime.now(), versionControlUser, folder1);

        // Act
        DirectoryStructureComponent directoryStructureComponent = commit.getDirectoryStructureComponent().getChild(0).getChild(0);

        // Assert
        assertEquals(file2.getName(), directoryStructureComponent.getName());
    }

    @Test
    void requestingDirectoryStructureComponentShouldThrowAnUnsupportedOperationException() {
        // Arrange
        DirectoryStructureComponent folder1 = new Folder("Folder 1");
        DirectoryStructureComponent folder2 = new Folder("Folder 2");
        DirectoryStructureComponent folder3 = new Folder("Folder 3");

        folder1.add(folder2);
        folder1.add(folder3);

        DirectoryStructureComponent file1 = new File("File 1", ".txt", 10);
        DirectoryStructureComponent file2 = new File("File 2", ".txt", 10);
        DirectoryStructureComponent file3 = new File("File 3", ".txt", 10);

        folder1.add(file1);
        folder2.add(file2);
        folder3.add(file3);

        VersionControlUser versionControlUser = new VersionControlUser("Tijgetje", "tijgetje@disney.com");
        Commit commit = new Commit("Initial commit", LocalDateTime.now(), versionControlUser, folder1);

        UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();

        // Act
        try {
            commit.getDirectoryStructureComponent().getChild(0).getChild(0).getChild(0);
        } catch (UnsupportedOperationException e) {
            unsupportedOperationException = e;
        }

        // Assert
        assertEquals("Unable to retrieve DirectoryStructureComponent", unsupportedOperationException.getMessage());
    }
}