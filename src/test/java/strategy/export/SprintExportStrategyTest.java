package strategy.export;

import adapter.sender.EmailAdapter;
import adapter.sender.MessageSender;
import domain.devops.*;
import library.EmailSender;
import org.junit.jupiter.api.Test;
import template.pipeline.DevelopmentPipeline;
import template.pipeline.TestPipeline;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SprintExportStrategyTest {
    @Test
    void SprintIsAbleToGenerateASummaryAsPDF() {
        // Arrange
        ProductBacklog productBacklog = new ProductBacklog();
        SprintBacklog sprint1Backlog = new SprintBacklog(null);
        MessageSender messageSender = new EmailAdapter(new EmailSender());

        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        List<ScrumTeamMember> scrumTeamMembers = new ArrayList<>();
        ScrumTeamMember developer = new Developer("Iejoor", "iejoor@disney.com", "+31612345678", messageSender);
        scrumTeamMembers.add(developer);
        ScrumTeamMember tester = new Tester("Uil", "uil@disney.com", "+31612345678", messageSender);
        scrumTeamMembers.add(tester);

        Project project = new Project("Avans", "DevOps", 1, productBacklog, productOwner);

        Sprint sprint = new RegularSprint(project,"Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), sprint1Backlog, scrumMaster, scrumTeamMembers, SprintExportType.PDF);
        sprint1Backlog.setSprint(sprint);
        DevelopmentPipeline developmentPipeline = new TestPipeline();
        sprint.setDevelopmentPipeline(developmentPipeline);

        ProductBacklogItem productBacklogItem1 = new ProductBacklogItem("Honing zoeken", "Een pot met honing zoeken", 2);
        ProductBacklogItem productBacklogItem2 = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 4);
        ProductBacklogItem productBacklogItem3 = new ProductBacklogItem("Blaadjes vangen", "Vallende blaadjes van een boom vangen", 2);
        ProductBacklogItem productBacklogItem4 = new ProductBacklogItem("Ballon maken", "Een ballon maken", 5);
        productBacklogItem1.setScrumTeamMember(developer);
        productBacklogItem2.setScrumTeamMember(developer);
        productBacklogItem4.setScrumTeamMember(tester);
        productBacklogItem3.setScrumTeamMember(tester);
        sprint1Backlog.add(productBacklogItem1);
        sprint1Backlog.add(productBacklogItem2);
        sprint1Backlog.add(productBacklogItem3);
        sprint1Backlog.add(productBacklogItem4);

        // Act
        sprint.export(true, true);

        StringBuilder stringBuilder = new StringBuilder();
        String fileName = "./exports/" + sprint.getProject().getProjectName() + "-" + sprint.getSprintName() + "-PDF.txt";
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String summary = stringBuilder.toString();

        // Assert
        assertEquals("DevOps-Sprint 1-PDF.txt", new File(fileName).getName());
        assertTrue(summary.contains("---------- HEADER ----------"));
        assertTrue(summary.contains("Iejoor, aantal punten: 6"));
        assertTrue(summary.contains("Uil, aantal punten: 7"));
        assertTrue(summary.contains("---------- FOOTER ----------"));
    }

    @Test
    void SprintIsAbleToGenerateASummaryAsPNG() {
        // Arrange
        ProductBacklog productBacklog = new ProductBacklog();
        SprintBacklog sprint1Backlog = new SprintBacklog(null);
        MessageSender messageSender = new EmailAdapter(new EmailSender());

        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        List<ScrumTeamMember> scrumTeamMembers = new ArrayList<>();
        ScrumTeamMember developer = new Developer("Iejoor", "iejoor@disney.com", "+31612345678", messageSender);
        scrumTeamMembers.add(developer);
        ScrumTeamMember tester = new Tester("Uil", "uil@disney.com", "+31612345678", messageSender);
        scrumTeamMembers.add(tester);

        Project project = new Project("Avans", "DevOps", 1, productBacklog, productOwner);

        Sprint sprint = new RegularSprint(project,"Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), sprint1Backlog, scrumMaster, scrumTeamMembers, SprintExportType.PNG);
        sprint1Backlog.setSprint(sprint);
        DevelopmentPipeline developmentPipeline = new TestPipeline();
        sprint.setDevelopmentPipeline(developmentPipeline);

        ProductBacklogItem productBacklogItem1 = new ProductBacklogItem("Honing zoeken", "Een pot met honing zoeken", 2);
        ProductBacklogItem productBacklogItem2 = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 4);
        ProductBacklogItem productBacklogItem3 = new ProductBacklogItem("Blaadjes vangen", "Vallende blaadjes van een boom vangen", 2);
        ProductBacklogItem productBacklogItem4 = new ProductBacklogItem("Ballon maken", "Een ballon maken", 5);
        productBacklogItem1.setScrumTeamMember(developer);
        productBacklogItem2.setScrumTeamMember(developer);
        productBacklogItem4.setScrumTeamMember(tester);
        productBacklogItem3.setScrumTeamMember(tester);
        sprint1Backlog.add(productBacklogItem1);
        sprint1Backlog.add(productBacklogItem2);
        sprint1Backlog.add(productBacklogItem3);
        sprint1Backlog.add(productBacklogItem4);

        // Act
        sprint.export(true, true);

        StringBuilder stringBuilder = new StringBuilder();
        String fileName = "./exports/" + sprint.getProject().getProjectName() + "-" + sprint.getSprintName() + "-PNG.txt";
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String summary = stringBuilder.toString();

        // Assert
        assertEquals("DevOps-Sprint 1-PNG.txt", new File(fileName).getName());
        assertTrue(summary.contains("---------- HEADER ----------"));
        assertTrue(summary.contains("Iejoor, aantal punten: 6"));
        assertTrue(summary.contains("Uil, aantal punten: 7"));
        assertTrue(summary.contains("---------- FOOTER ----------"));
    }
}