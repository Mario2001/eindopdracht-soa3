package template.pipeline;

import factory.pipeline.DevelopmentPipelineCreator;
import factory.pipeline.UnitTestPipelineCreator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UnitTestPipelineTest {
    @Test
    void unitTestPipelineRunsSuccessfully() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new UnitTestPipelineCreator();
        UnitTestPipeline developmentPipeline = (UnitTestPipeline) developmentPipelineCreator.createDevelopmentPipeline();

        // Act
        developmentPipeline.execute();

        // Assert
        assertTrue(developmentPipeline.hasReachedInstallPackagesStage());
        assertTrue(developmentPipeline.hasReachedBuildStage());
        assertTrue(developmentPipeline.hasReachedTestStage());
        assertTrue(developmentPipeline.hasReachedPrepareAnalysisStage());
        assertTrue(developmentPipeline.hasReachedExecuteAnalysisStage());
        assertTrue(developmentPipeline.hasReachedReportAnalysisStage());
        assertTrue(developmentPipeline.hasReachedDeployStage());
        assertTrue(developmentPipeline.hasReachedExecuteUtilitiesStage());
    }
}