package observer;

import adapter.sender.EmailAdapter;
import adapter.sender.MessageSender;
import domain.devops.*;
import factory.pipeline.DefectPipelineCreator;
import factory.pipeline.DevelopmentPipelineCreator;
import factory.pipeline.TestPipelineCreator;
import library.EmailSender;
import org.junit.jupiter.api.Test;
import strategy.export.SprintExportType;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class SprintSubjectTest {
    @Test
    void scrumMasterShouldReceiveNotificationAfterFailedDevelopmentPipelineFromFinishedState() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);

        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.releaseSprint();

        // Assert
        assertEquals("Knorretje, RegularSprint, Pipeline failed for sprint: Sprint 1", scrumMaster.getMessage(0));
    }

    @Test
    void scrumMasterShouldReceiveNotificationAfterFailedDevelopmentPipelineFromReviewState() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);

        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.reviewSprint();
        sprint.releaseSprint();

        // Assert
        assertEquals("Knorretje, RegularSprint, Pipeline failed for sprint: Sprint 1", scrumMaster.getMessage(0));
    }

    @Test
    void scrumMasterShouldReceiveNotificationAfterFailedDevelopmentPipelineFromFailedReleaseState() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);

        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.releaseSprint();
        sprint.releaseSprint();

        // Assert
        assertEquals("Knorretje, RegularSprint, Pipeline failed for sprint: Sprint 1", scrumMaster.getMessage(1));
    }

    @Test
    void scrumMasterAndProductOwnerShouldReceiveNotificationWhenSprintReleaseIsCancelledFromReviewState() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new TestPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);

        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.reviewSprint();
        sprint.cancelSprintRelease();

        // Assert
        assertEquals("Knorretje, RegularSprint, Sprint: Sprint 1 Sprint release cancelled", scrumMaster.getMessage(0));
        assertEquals("Winnie de Poeh, RegularSprint, Sprint: Sprint 1 Sprint release cancelled", productOwner.getMessage(0));
    }

    @Test
    void scrumMasterAndProductOwnerShouldReceiveNotificationWhenSprintReleaseIsCancelledFromFailedReleaseState() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);

        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.reviewSprint();
        sprint.releaseSprint();
        sprint.cancelSprintRelease();

        // Assert
        assertEquals("Knorretje, RegularSprint, Sprint: Sprint 1 Sprint release cancelled", scrumMaster.getMessage(1));
        assertEquals("Winnie de Poeh, RegularSprint, Sprint: Sprint 1 Sprint release cancelled", productOwner.getMessage(0));
    }

    // -----------------------------------------------------

    @Test
    void scrumMasterAndProductOwnerShouldReceiveNotificationAfterSuccessfulDevelopmentPipelineExecutionFromFinishedState() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new TestPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);

        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.releaseSprint();

        // Assert
        assertEquals("Knorretje, RegularSprint, Pipeline success for sprint: Sprint 1", scrumMaster.getMessage(0));
        assertEquals("Winnie de Poeh, RegularSprint, Pipeline success for sprint: Sprint 1", productOwner.getMessage(0));
    }

    @Test
    void scrumMasterAndProductOwnerShouldReceiveNotificationAfterSuccessfulDevelopmentPipelineExecutionFromReviewState() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new TestPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);

        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.reviewSprint();
        sprint.releaseSprint();

        // Assert
        assertEquals("Knorretje, RegularSprint, Pipeline success for sprint: Sprint 1", scrumMaster.getMessage(0));
        assertEquals("Winnie de Poeh, RegularSprint, Pipeline success for sprint: Sprint 1", productOwner.getMessage(0));
    }

    @Test
    void scrumMasterAndProductOwnerShouldReceiveNotificationAfterSuccessfulDevelopmentPipelineExecutionFromFailedReleaseState() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);

        Project project = new Project("Avans", "DevOps", 1, null, productOwner);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, new ArrayList<>(), SprintExportType.PDF);
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        // Act
        sprint.startSprint();
        sprint.finishSprint();
        sprint.releaseSprint();

        developmentPipelineCreator = new TestPipelineCreator();
        sprint.setDevelopmentPipeline(developmentPipelineCreator.createDevelopmentPipeline());

        sprint.releaseSprint();

        // Assert
        assertEquals("Knorretje, RegularSprint, Pipeline success for sprint: Sprint 1", scrumMaster.getMessage(1));
        assertEquals("Winnie de Poeh, RegularSprint, Pipeline success for sprint: Sprint 1", productOwner.getMessage(0));
    }
}