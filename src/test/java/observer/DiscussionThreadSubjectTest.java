package observer;

import adapter.sender.EmailAdapter;
import adapter.sender.MessageSender;
import domain.devops.*;
import library.EmailSender;
import org.junit.jupiter.api.Test;
import strategy.export.SprintExportType;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DiscussionThreadSubjectTest {
    @Test
    void allParticipantsOfADiscussionThreadShouldReceiveNotificationWhenACommentIsAdded() {
        // Arrange
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        DiscussionThread discussionThread = new DiscussionThread("Conflicterende requirement", "Conflicterende requirement oplossen", productBacklogItem);
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ProductOwner productOwner = new ProductOwner("Winnie de Poeh", "poehbeer@disney.com", "+31612345678", messageSender);
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);

        // Act
        discussionThread.add(new Comment("Meer honing!", productOwner));

        assertTrue(scrumMaster.getMessages().isEmpty());

        discussionThread.add(new Comment("Goed idee.", scrumMaster));

        // Assert
        assertEquals("Winnie de Poeh, DiscussionThread, New message in Conflicterende requirement: Meer honing!", productOwner.getMessage(0));
        assertEquals("Winnie de Poeh, DiscussionThread, New message in Conflicterende requirement: Goed idee.", productOwner.getMessage(1));
        assertEquals("Knorretje, DiscussionThread, New message in Conflicterende requirement: Goed idee.", scrumMaster.getMessage(0));
    }

    @Test
    void aDiscussionThreadCanBeAddedToAProductBacklogItem() {
        // Arrange
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);

        // Act
        DiscussionThread discussionThread = new DiscussionThread("Conflicterende requirement", "Conflicterende requirement oplossen", null);
        productBacklogItem.add(discussionThread);

        // Assert
        assertFalse(productBacklogItem.getDiscussionThreads().isEmpty());
    }

    @Test
    void aDiscussionThreadCannotBeAddedToAProductBacklogItem() {
        // Arrange
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, null, new ArrayList<>(), SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        productBacklogItem.setSprintBacklog(sprintBacklog);
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);

        productBacklogItem.setToDoing(scrumTeamMember);
        productBacklogItem.setToReadyForTesting();
        productBacklogItem.setToTesting();
        productBacklogItem.setToTested();
        productBacklogItem.setToDone();

        // Act
        DiscussionThread discussionThread = new DiscussionThread("Conflicterende requirement", "Conflicterende requirement oplossen", productBacklogItem);
        productBacklogItem.add(discussionThread);

        // Assert
        assertTrue(productBacklogItem.getDiscussionThreads().isEmpty());
    }

    @Test
    void aDiscussionThreadCanBeRemovedFromAProductBacklogItem() {
        // Arrange
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        DiscussionThread discussionThread = new DiscussionThread("Conflicterende requirement", "Conflicterende requirement oplossen", productBacklogItem);
        productBacklogItem.add(discussionThread);

        // Act
        productBacklogItem.removeDiscussionThread(0);

        // Assert
        assertTrue(productBacklogItem.getDiscussionThreads().isEmpty());
    }

    @Test
    void aDiscussionThreadCannotBeRemovedFromAProductBacklogItem() {
        // Arrange
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, null, new ArrayList<>(), SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        productBacklogItem.setSprintBacklog(sprintBacklog);
        DiscussionThread discussionThread = new DiscussionThread("Conflicterende requirement", "Conflicterende requirement oplossen", productBacklogItem);
        productBacklogItem.add(discussionThread);
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);

        productBacklogItem.setToDoing(scrumTeamMember);
        productBacklogItem.setToReadyForTesting();
        productBacklogItem.setToTesting();
        productBacklogItem.setToTested();
        productBacklogItem.setToDone();

        // Act
        productBacklogItem.removeDiscussionThread(0);

        // Assert
        assertFalse(productBacklogItem.getDiscussionThreads().isEmpty());
    }

    @Test
    void aCommentCanBeAddedToADiscussionThread() {
        // Arrange
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        DiscussionThread discussionThread = new DiscussionThread("Conflicterende requirement", "Conflicterende requirement oplossen", productBacklogItem);
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);

        // Act
        discussionThread.add(new Comment("Meer honing!", scrumTeamMember));

        // Assert
        assertFalse(discussionThread.getComments().isEmpty());
    }

    @Test
    void aCommentCannotBeAddedToADiscussionThread() {
        // Arrange
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, null, new ArrayList<>(), SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        productBacklogItem.setSprintBacklog(sprintBacklog);
        DiscussionThread discussionThread = new DiscussionThread("Conflicterende requirement", "Conflicterende requirement oplossen", productBacklogItem);
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);

        productBacklogItem.setToDoing(scrumTeamMember);
        productBacklogItem.setToReadyForTesting();
        productBacklogItem.setToTesting();
        productBacklogItem.setToTested();
        productBacklogItem.setToDone();

        // Act
        discussionThread.add(new Comment("Meer honing!", scrumTeamMember));

        // Assert
        assertTrue(discussionThread.getComments().isEmpty());
    }

    @Test
    void aCommentCanBeRemovedFromADiscussionThread() {
        // Arrange
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        DiscussionThread discussionThread = new DiscussionThread("Conflicterende requirement", "Conflicterende requirement oplossen", productBacklogItem);
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);
        discussionThread.add(new Comment("Meer honing!", scrumTeamMember));

        // Act
        discussionThread.removeComment(0);

        // Assert
        assertTrue(discussionThread.getComments().isEmpty());
    }

    @Test
    void aCommentCannotBeRemovedFromADiscussionThread() {
        // Arrange
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, null, new ArrayList<>(), SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        productBacklogItem.setSprintBacklog(sprintBacklog);
        DiscussionThread discussionThread = new DiscussionThread("Conflicterende requirement", "Conflicterende requirement oplossen", productBacklogItem);
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember scrumTeamMember = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);
        discussionThread.add(new Comment("Meer honing!", scrumTeamMember));

        productBacklogItem.setToDoing(scrumTeamMember);
        productBacklogItem.setToReadyForTesting();
        productBacklogItem.setToTesting();
        productBacklogItem.setToTested();
        productBacklogItem.setToDone();

        // Act
        discussionThread.removeComment(0);

        // Assert
        assertFalse(discussionThread.getComments().isEmpty());
    }
}
