package observer;

import adapter.sender.EmailAdapter;
import adapter.sender.MessageSender;
import domain.devops.*;
import library.EmailSender;
import org.junit.jupiter.api.Test;
import strategy.export.SprintExportType;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ProductBacklogItemSubjectTest {
    @Test
    void allTestersShouldReceiveNotificationWhenProductBacklogItemHasReadyForTestingStateFromDoingState() {
        // Arrange
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember developer = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);
        Tester tester1 = new Tester("Iejoor", "iejoor@disney.com", "+31612345678", messageSender);
        Tester tester2 = new Tester("Uil", "uil@disney.com", "+31612345678", messageSender);
        List<ScrumTeamMember> scrumTeamMembers = new ArrayList<>();
        scrumTeamMembers.add(developer);
        scrumTeamMembers.add(tester1);
        scrumTeamMembers.add(tester2);

        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, null, scrumTeamMembers, SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        productBacklogItem.setSprintBacklog(sprintBacklog);

        // Act
        productBacklogItem.setToDoing(developer);
        productBacklogItem.setToReadyForTesting();

        // Assert
        assertEquals("Iejoor, ProductBacklogItem, New item for testing", tester1.getMessage(0));
        assertEquals("Uil, ProductBacklogItem, New item for testing", tester2.getMessage(0));
        assertTrue(developer.getMessages().isEmpty());
    }

    @Test
    void allTestersShouldReceiveNotificationWhenProductBacklogItemHasReadyForTestingStateFromTestedState() {
        // Arrange
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumTeamMember developer = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);
        Tester tester1 = new Tester("Iejoor", "iejoor@disney.com", "+31612345678", messageSender);
        Tester tester2 = new Tester("Uil", "uil@disney.com", "+31612345678", messageSender);
        List<ScrumTeamMember> scrumTeamMembers = new ArrayList<>();
        scrumTeamMembers.add(developer);
        scrumTeamMembers.add(tester1);
        scrumTeamMembers.add(tester2);

        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, null, scrumTeamMembers, SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        productBacklogItem.setSprintBacklog(sprintBacklog);

        // Act
        productBacklogItem.setToDoing(developer);
        productBacklogItem.setToReadyForTesting();
        productBacklogItem.setToTesting();
        productBacklogItem.setToTested();
        productBacklogItem.setToReadyForTesting();

        // Assert
        assertEquals("Iejoor, ProductBacklogItem, New item for testing", tester1.getMessage(1));
        assertEquals("Uil, ProductBacklogItem, New item for testing", tester2.getMessage(1));
        assertTrue(developer.getMessages().isEmpty());
    }

    @Test
    void scrumMasterShouldReceiveNotificationOnNegativeTestResult() {
        // Arrange
        MessageSender messageSender = new EmailAdapter(new EmailSender());
        ScrumMaster scrumMaster = new ScrumMaster("Knorretje", "knorretje@disney.com", "+31612345678", messageSender);
        ScrumTeamMember developer = new Developer("Tijgetje", "tijgetje@disney.com", "+31612345678", messageSender);
        Tester tester1 = new Tester("Iejoor", "iejoor@disney.com", "+31612345678", messageSender);
        Tester tester2 = new Tester("Uil", "uil@disney.com", "+31612345678", messageSender);
        List<ScrumTeamMember> scrumTeamMembers = new ArrayList<>();
        scrumTeamMembers.add(scrumMaster);
        scrumTeamMembers.add(developer);
        scrumTeamMembers.add(tester1);
        scrumTeamMembers.add(tester2);

        ProductBacklogItem productBacklogItem = new ProductBacklogItem("Honing maken", "Een pot met honing maken", 2);
        Project project = new Project("Avans", "DevOps", 1, null, null);
        Sprint sprint = new RegularSprint(project, "Sprint 1", "Requirements verzamelen", LocalDateTime.now(), LocalDateTime.now().plusDays(7), null, scrumMaster, scrumTeamMembers, SprintExportType.PDF);
        SprintBacklog sprintBacklog = new SprintBacklog(sprint);
        productBacklogItem.setSprintBacklog(sprintBacklog);

        // Act
        productBacklogItem.setToDoing(developer);
        productBacklogItem.setToReadyForTesting();
        productBacklogItem.setToTesting();
        productBacklogItem.setToToDo();

        // Assert
        assertEquals("Knorretje, ProductBacklogItem, Item rejected from testing back to todo", scrumMaster.getMessage(0));
    }
}
