package factory.pipeline;

import org.junit.jupiter.api.Test;
import template.pipeline.*;

import static org.junit.jupiter.api.Assertions.*;

class DevelopmentPipelineCreatorTest {
    @Test
    void createDevelopmentPipelineShouldReturnANewDefectPipelineInstance() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DefectPipelineCreator();

        // Act
        DevelopmentPipeline defectPipeline = developmentPipelineCreator.createDevelopmentPipeline();

        // Assert
        assertEquals(DefectPipeline.class, defectPipeline.getClass());
    }

    @Test
    void createDevelopmentPipelineShouldReturnANewDeploymentPipelineInstance() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new DeploymentPipelineCreator();

        // Act
        DevelopmentPipeline defectPipeline = developmentPipelineCreator.createDevelopmentPipeline();

        // Assert
        assertEquals(DeploymentPipeline.class, defectPipeline.getClass());
    }

    @Test
    void createDevelopmentPipelineShouldReturnANewTestPipelineInstance() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new TestPipelineCreator();

        // Act
        DevelopmentPipeline defectPipeline = developmentPipelineCreator.createDevelopmentPipeline();

        // Assert
        assertEquals(TestPipeline.class, defectPipeline.getClass());
    }

    @Test
    void createDevelopmentPipelineShouldReturnANewUnitTestPipelineInstance() {
        // Arrange
        DevelopmentPipelineCreator developmentPipelineCreator = new UnitTestPipelineCreator();

        // Act
        DevelopmentPipeline defectPipeline = developmentPipelineCreator.createDevelopmentPipeline();

        // Assert
        assertEquals(UnitTestPipeline.class, defectPipeline.getClass());
    }
}